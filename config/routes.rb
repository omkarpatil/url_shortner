Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: "urls#new"

  resources :urls, only: [:index, :new, :create] do
    member do
      get :details
    end
  end

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :urls, only: [:index, :create, :show, :destroy]
    end
  end

  match '/:key', to: 'urls#show', as: 'shortned', via: [:get, :post]
end
