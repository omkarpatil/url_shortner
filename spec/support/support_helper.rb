module SupportHelper
  def response_body(object_class: OpenStruct)
    JSON.parse(response.body, object_class: object_class)
  end
end
