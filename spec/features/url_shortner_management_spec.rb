require 'rails_helper'

RSpec.feature "URL Shortner Management", type: :feature do
  scenario 'user enters valid url' do
    visit "/"

    expect(page).to have_http_status :ok

    fill_in 'url', with: 'http://www.google.com'

    click_button 'SHORTEN URL'

    expect(page).to have_http_status :ok

    expect(page).to have_content 'http://google.com'
    expect(page).to have_content 'baea95'

    visit '/baea95'

    expect(current_url).to eql('http://google.com/')
  end

  scenario 'user enters invalid url' do
    visit "/"

    expect(page).to have_http_status :ok

    fill_in 'url', with: 'invalid url'

    click_button 'SHORTEN URL'

    expect(page).to have_content 'Url is invalid'
  end

  scenario 'user do not enter url' do
    visit "/"

    expect(page).to have_http_status :ok

    click_button 'SHORTEN URL'

    expect(page).to have_content "Url can't be blank"
  end
end
