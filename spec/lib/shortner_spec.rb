require 'rails_helper'
require_relative '../../lib/shortner'

RSpec.describe Shortner do

  subject(:shortner) { Shortner.new('http://www.google.com') }

  context 'when http://www.google.com is passed' do
    describe '#key_length' do
      it 'set key_length to 6' do
        expect(shortner.key_length).to eql(6)
      end

      it 'return key with 6 characters' do
        expect(shortner.key.length).to eql(6)
      end
    end

    describe '#clean_url' do
      it 'clean_url is equal to http://google.com' do
        expect(shortner.clean_url).to eql("http://google.com")
      end
    end

    describe '#key' do
      it 'key is equal to baea95' do
        expect(shortner.key).to eql("baea95")
      end
    end
  end

  context 'when different types of urls are passed of same domain' do
    let(:urls) do
      ['google.com', 'www.google.com', 'http://google.com',  'http://www.google.com', 'https://google.com', 'https://www.google.com']
    end
    describe '#key' do
      it 'generate same key for all the urls of same type' do
        key_count = urls.map { |url| Shortner.new(url).key }.uniq.count
        expect(key_count).to eql(1)
      end
    end
  end

end
