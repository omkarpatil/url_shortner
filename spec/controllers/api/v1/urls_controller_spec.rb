require "rails_helper"

RSpec.describe Api::V1::UrlsController, :type => :controller do
  render_views

  describe "GET index" do
    before { get :index, format: :json }

    it { expect(response).to have_http_status :ok }
    it { expect(response_body.status).to eql('success') }
    it { expect(response_body.data.try(:urls)).to be_an(Array) }
  end

  describe "POST create" do
    context 'when valid url is sent' do

      before { post :create, params: { url: 'http://google.com' }, format: :json }

      it { expect(response).to have_http_status :created }
      it { expect(response_body.status).to eql('success') }
      it { expect(response_body.data.try(:key)).to eql('baea95') }
    end

    context 'when invalid url is sent' do

      before { post :create, params: { url: 'invalid url' }, format: :json }

      it { expect(response).to have_http_status :unprocessable_entity }
      it { expect(response_body.status).to eql('failed') }
      it { expect(response_body.type).to eql('ArgumentError') }
      it { expect(response_body.errors.try(:full_messages)).to include('Url is invalid') }
    end

    context 'when url is not sent in params' do

      before { post :create, params: { url: '' }, format: :json }

      it { expect(response).to have_http_status :unprocessable_entity }
      it { expect(response_body.status).to eql('failed') }
      it { expect(response_body.type).to eql('ArgumentError') }
      it { expect(response_body.errors.try(:full_messages)).to include("Url can't be blank") }
    end
  end

  describe "DELETE destroy" do
    let(:url) { FactoryBot.create(:url) }
    before { delete :destroy, params: { id: url.id }, format: :json }

    it { expect(response).to have_http_status :ok }
    it { expect(response_body.status).to eql('success') }
  end

  describe "GET show" do
    let(:url) { FactoryBot.create(:url) }
    before { get :show, params: { id: url.id }, format: :json }

    it { expect(response).to have_http_status :ok }
    it { expect(response_body.status).to eql('success') }
    it { expect(response_body.data.try(:id)).to eql(url.id) }
    it { expect(response_body.data.try(:key)).to eql(url.key) }
    it { expect(response_body.data.try(:target_url)).to eql(url.url) }
    it { expect(response_body.data.try(:shortened_url)).to eql("http://test.host/#{url.key}") }
  end
end