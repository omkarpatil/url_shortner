class Shortner
  attr_reader :url, :key_length

  HOSTNAME_CLEAN_REGEX = /^www./
  PATH_CLEAN_REGEX = /\/$/
  SCHEME_IDENTIFY_REGEX = /^http:\/\/|^https:\/\//
  DEFAULT_SCHEME = "http"

  def initialize(url, key_length: 6)
    @key_length = 6
    @url = URI.escape(url)
  end

  def clean_url
    @clean_url ||= get_clean_url
  end

  def key
    Digest::SHA1.hexdigest(get_id).first(key_length)
  end

  private

    def clean_hostname(hostname)
      hostname.gsub(HOSTNAME_CLEAN_REGEX, '')
    end

    def clean_path(path)
      path.gsub(PATH_CLEAN_REGEX, '')
    end

    def url_with_scheme?(url)
      url.scan(SCHEME_IDENTIFY_REGEX).present?
    end

    def fetch_scheme(uri)
      uri.scheme.presence || DEFAULT_SCHEME
    end

    def schemed_url(url)
      unless url_with_scheme?(url)
      "#{DEFAULT_SCHEME}://#{url}"
      else
        url
      end
    end

    def get_clean_url
      uri = URI(schemed_url(url))
      query = uri.query
      scheme = fetch_scheme(uri)
      hostname = clean_hostname(uri.hostname)
      path = clean_path(uri.path)

      cleaned_path = "#{scheme}://#{hostname}#{clean_path(uri.path)}"
      query.present? ? cleaned_path.concat("?#{query}") : cleaned_path
    end

    def get_id
      uri = URI(schemed_url(url))
      query = uri.query
      scheme = fetch_scheme(uri)
      hostname = clean_hostname(uri.hostname)
      path = clean_path(uri.path)

      cleaned_path = "#{hostname}#{clean_path(uri.path)}"
      query.present? ? cleaned_path.concat("?#{query}") : cleaned_path
    end
end
