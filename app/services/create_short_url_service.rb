require 'shortner'

class CreateShortUrlService < ::BaseService
  attr_accessor :url, :url_object

  validates_presence_of :url
  validate :url_cannot_be_invalid

  def intialize(url: nil)
    @url = url
  end

  def call
    shortner = Shortner.new(url)
    @url_object = Url.find_or_initialize_by(key: shortner.key)
    @url_object.update_attributes(url: shortner.clean_url)
    true
  end

  def url_cannot_be_invalid
    return nil if self.url.blank?
    URI.parse(url)
  rescue URI::InvalidURIError => iue
    errors.add(:url, "is invalid")
  end

end
