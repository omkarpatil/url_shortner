class BaseService
  include ActiveModel::Model

  def error_body
    {
      messages: self.errors.messages,
      full_messages: self.errors.full_messages
    }
  end

  def valid?
    before_validation
    super
  end

  def before_validation
  end

end
