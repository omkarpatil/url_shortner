json.data do
  json.partial! 'api/v1/urls/url', url: @url
end
json.status 'success'