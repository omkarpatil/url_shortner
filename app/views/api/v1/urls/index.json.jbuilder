json.data do
  json.urls @urls.each do |url|
    json.partial! 'api/v1/urls/url', url: url
  end
end
json.status 'success'