class Api::V1::UrlsController < Api::V1::VersionController
  before_action :set_url, only: [:show, :destroy]

  def index
    @urls = Url.all
  end

  def show
  end

  def destroy
    @url.present? && @url.destroy
  end

  def create
    service = CreateShortUrlService.new(url: params[:url].to_s)

    if service.valid?
      service.call
      @url = service.url_object
      render status: :created
    else
      body = error_response(message: service.errors.full_messages.join,
        errors: service.error_body, type: 'ArgumentError', code: 1)
      render json: body, status: :unprocessable_entity
    end
  end

  private

    def set_url
      @url = Url.find(params[:id])
    end
end
