class Api::ApiController < ApplicationController
  skip_before_action :verify_authenticity_token

  rescue_from Exception, with: :internal_server_error
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

  def record_not_found(exception)
    error = error_response(
      message: exception.to_s.split(' with').first,
      type: 'ResourceNotFound',
      data: { backtrace: exception.backtrace.unshift(exception.to_s) },
      code: 404
    )
    render json: error, status: 404
  end


  private

    def error_response(message:, type:, code: nil, trace_id: nil, status: 'failed', data: nil, errors: nil)
      {
        data: data,
        errors: errors,
        type: type, 
        code: code,
        message: message,
        status: status
      }
    end

    def success_response(data: nil, message: nil, status: 'success')
      { data: data, message: message, status: status }
    end

    def internal_server_error(exception)
      response = error_response(
        message: 'Something Went Wrong!',
        type: 'InternalServerError',
        data: { backtrace: exception.backtrace.unshift(exception.to_s) },
        code: 500
      )
      render json: response, status: 500
    end

end
