class UrlsController < ApplicationController
  before_action :set_url, only: [:details]

  def index
    @urls = Url.all
  end

  def new
  end

  def create
    service = CreateShortUrlService.new(url: params[:url])

    if service.valid?
      service.call
      redirect_to details_url_path(service.url_object)
    else
      flash.now[:error] = service.errors.full_messages.join(', ')
      render :new
    end
  end

  def details
  end

  def show
    @url = Url.find_by(key: params[:key])
    redirect_to @url.url
  end

  private

    def set_url
      @url = Url.find(params[:id])
    end

end
